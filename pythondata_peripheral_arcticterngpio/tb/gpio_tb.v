// © 2022 Raptor Engineering, LLC
//
// Released under the terms of the LGPL v3+
// See the LICENSE file for full details

module gpio_tb;
	wire [17:0] gpio_in;
	wire [17:0] gpio_out;
	reg [17:0] gpio_direction = 0;
	reg gpio_tx_clock = 0;
	reg gpio_tx_reset = 0;

	wire scl_out;
	wire scl_in;
	wire scl_dir;

	wire sda_out;
	wire sda_in;
	wire sda_dir;

	reg dut_sda_drive = 0;

	assign sda_in = ((sda_dir)?sda_out:1'b1)&dut_sda_drive;
	assign scl_in = (scl_dir)?scl_out:1'b1;

	smd_hex_output smd_hex_output (
		.anodes(gpio_in[13:0]),
		.cathodes(gpio_in[17:14]),

		.display_word(16'h1234),

		.clk(gpio_tx_clock)
	);

	gpio_tca6418_bridge #(
		.PERIPHERAL_CLOCK_FREQUENCY(50000000),
		.I2C_BUS_CLOCK_FREQUENCY(400000)
	) gpio_tca6418_bridge (
		.gpio_in(gpio_in),
		.gpio_out(gpio_out),
		.gpio_direction(gpio_direction),

		.scl_out(scl_out),
		.scl_in(scl_in),
		.scl_dir(scl_dir),

		.sda_out(sda_out),
		.sda_in(sda_in),
		.sda_dir(sda_dir),

		.peripheral_clock(gpio_tx_clock),
		.peripheral_reset(gpio_tx_reset)
	);

	initial begin
		$dumpfile("gpio_test.vcd");
		$dumpvars(0, gpio_tb);
	end

	initial begin
		dut_sda_drive <= 1'b1;
		gpio_tx_reset <= 1'b1;
		#100
		gpio_tx_reset <= 1'b0;
		gpio_direction <= 17'b11111111111111111;
		#132000
		dut_sda_drive <= 1'b0;
		#14000
		dut_sda_drive <= 1'b1;
		#400000000
		$finish;
	end

	always begin
		#50 gpio_tx_clock = ~gpio_tx_clock;
	end
endmodule
