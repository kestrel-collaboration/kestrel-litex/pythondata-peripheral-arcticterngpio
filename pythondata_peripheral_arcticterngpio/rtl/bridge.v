// © 2022 Raptor Engineering, LLC
//
// Released under the terms of the LGPL v3+
// See the LICENSE file for full details

module gpio_tca6418_bridge(
		input wire [17:0] gpio_in,
		output wire [17:0] gpio_out,
		input wire [17:0] gpio_direction,

		output wire scl_out,
		input wire scl_in,
		output wire scl_dir,

		output wire sda_out,
		input wire sda_in,
		output wire sda_dir,

		output wire digit_sync,

		input wire peripheral_clock,
		input wire peripheral_reset
	);

	parameter PERIPHERAL_CLOCK_FREQUENCY = 50000000;
	parameter I2C_BUS_CLOCK_FREQUENCY    = 400000;
	parameter OPERATION_TIMEOUT_CYCLES   = 65536;
	parameter GPIO_CONTROLLER_ADDRESS    = 8'h34;
	parameter GPIO_CTRL_REG_DAT_OUT1     = 8'h17;
	parameter GPIO_CTRL_REG_DIR1         = 8'h23;
	parameter GPIO_DAT_STAT1             = 8'h14;

	reg i2c_ctl_start = 0;
	reg i2c_ctl_stop = 0;
	reg i2c_ctl_read = 0;
	reg i2c_ctl_write = 0;
	reg i2c_ctl_ack = 0;
	wire i2c_sta_busy;
	wire i2c_sta_arb_lost;
	wire i2c_sta_ack;
	wire i2c_cmd_ack;
	wire [7:0] i2c_data_read;
	reg [7:0] i2c_data_write = 0;
	reg [17:0] gpio_in_reg = 0;
	reg [17:0] gpio_direction_reg = 0;
	reg [17:0] gpio_out_reg = 0;

	// Work around assumptions made in LiteX GPIO subsystem
	assign gpio_out[17] = gpio_direction[17] ? gpio_in[17] : gpio_out_reg[17];
	assign gpio_out[16] = gpio_direction[16] ? gpio_in[16] : gpio_out_reg[16];
	assign gpio_out[15] = gpio_direction[15] ? gpio_in[15] : gpio_out_reg[15];
	assign gpio_out[14] = gpio_direction[14] ? gpio_in[14] : gpio_out_reg[14];
	assign gpio_out[13] = gpio_direction[13] ? gpio_in[13] : gpio_out_reg[13];
	assign gpio_out[12] = gpio_direction[12] ? gpio_in[12] : gpio_out_reg[12];
	assign gpio_out[11] = gpio_direction[11] ? gpio_in[11] : gpio_out_reg[11];
	assign gpio_out[10] = gpio_direction[10] ? gpio_in[10] : gpio_out_reg[10];
	assign gpio_out[9] = gpio_direction[9] ? gpio_in[9] : gpio_out_reg[9];
	assign gpio_out[8] = gpio_direction[8] ? gpio_in[8] : gpio_out_reg[8];
	assign gpio_out[7] = gpio_direction[7] ? gpio_in[7] : gpio_out_reg[7];
	assign gpio_out[6] = gpio_direction[6] ? gpio_in[6] : gpio_out_reg[6];
	assign gpio_out[5] = gpio_direction[5] ? gpio_in[5] : gpio_out_reg[5];
	assign gpio_out[4] = gpio_direction[4] ? gpio_in[4] : gpio_out_reg[4];
	assign gpio_out[3] = gpio_direction[3] ? gpio_in[3] : gpio_out_reg[3];
	assign gpio_out[2] = gpio_direction[2] ? gpio_in[2] : gpio_out_reg[2];
	assign gpio_out[1] = gpio_direction[1] ? gpio_in[1] : gpio_out_reg[1];
	assign gpio_out[0] = gpio_direction[0] ? gpio_in[0] : gpio_out_reg[0];

	reg digit_sync_reg = 0;
	assign digit_sync = digit_sync_reg;

	wire i2c_scl_oen;
	wire i2c_sda_oen;

	assign scl_dir = ~i2c_scl_oen;
	assign sda_dir = ~i2c_sda_oen;

	// Instantiate I2C byte controller
	i2c_master_byte_ctrl byte_controller (
		.clk(peripheral_clock),
		.rst(peripheral_reset),
		.nReset(~peripheral_reset),
		.ena(1'b1),
		.clk_cnt((PERIPHERAL_CLOCK_FREQUENCY / (5 * I2C_BUS_CLOCK_FREQUENCY)) - 1),
		.start(i2c_ctl_start),
		.stop(i2c_ctl_stop),
		.read(i2c_ctl_read),
		.write(i2c_ctl_write),
		.ack_in(i2c_ctl_ack),
		.din(i2c_data_write),
		.cmd_ack(i2c_cmd_ack),
		.ack_out(i2c_sta_ack),
		.dout(i2c_data_read),
		.i2c_busy(i2c_sta_busy),
		.i2c_al(i2c_sta_arb_lost),
		.scl_i(scl_in),
		.scl_o(scl_out),
		.scl_oen(i2c_scl_oen),
		.sda_i(sda_in),
		.sda_o(sda_out),
		.sda_oen(i2c_sda_oen)
	);

	wire transaction_in_progress;
	assign transaction_in_progress = i2c_ctl_read | i2c_ctl_write;

	parameter GPIO_INITIALIZE_STATE_01 = 0;
	parameter GPIO_TRANSFER_STATE_ER01 = 1;
	parameter GPIO_TRANSFER_STATE_ER02 = 2;

	parameter GPIO_TRANSFER_STATE_IDLE = 16;
	parameter GPIO_TRANSFER_STATE_TX01 = 17;
	parameter GPIO_TRANSFER_STATE_TX02 = 18;
	parameter GPIO_TRANSFER_STATE_TX03 = 19;
	parameter GPIO_TRANSFER_STATE_TX04 = 20;
	parameter GPIO_TRANSFER_STATE_TX05 = 21;
	parameter GPIO_TRANSFER_STATE_TX06 = 22;
	parameter GPIO_TRANSFER_STATE_TX07 = 23;
	parameter GPIO_TRANSFER_STATE_TX08 = 24;
	parameter GPIO_TRANSFER_STATE_TX09 = 25;
	parameter GPIO_TRANSFER_STATE_TX10 = 26;
	parameter GPIO_TRANSFER_STATE_TX11 = 27;
	parameter GPIO_TRANSFER_STATE_TX12 = 28;
	parameter GPIO_TRANSFER_STATE_TX13 = 29;
	parameter GPIO_TRANSFER_STATE_TX14 = 30;
	parameter GPIO_TRANSFER_STATE_TX15 = 31;
	parameter GPIO_TRANSFER_STATE_TX16 = 32;
	parameter GPIO_TRANSFER_STATE_TX17 = 33;
	parameter GPIO_TRANSFER_STATE_TX18 = 34;
	parameter GPIO_TRANSFER_STATE_TX19 = 35;
	parameter GPIO_TRANSFER_STATE_TX20 = 36;
	parameter GPIO_TRANSFER_STATE_TX21 = 37;
	parameter GPIO_TRANSFER_STATE_TX22 = 38;
	parameter GPIO_TRANSFER_STATE_TX23 = 39;

	parameter GPIO_TRANSFER_STATE_DR00 = 48;
	parameter GPIO_TRANSFER_STATE_DR01 = 49;
	parameter GPIO_TRANSFER_STATE_DR02 = 50;
	parameter GPIO_TRANSFER_STATE_DR03 = 51;
	parameter GPIO_TRANSFER_STATE_DR04 = 52;
	parameter GPIO_TRANSFER_STATE_DR05 = 53;
	parameter GPIO_TRANSFER_STATE_DR06 = 54;
	parameter GPIO_TRANSFER_STATE_DR07 = 55;
	parameter GPIO_TRANSFER_STATE_DR08 = 56;
	parameter GPIO_TRANSFER_STATE_DR09 = 57;
	parameter GPIO_TRANSFER_STATE_DR10 = 58;
	parameter GPIO_TRANSFER_STATE_DR11 = 59;
	parameter GPIO_TRANSFER_STATE_DR12 = 60;
	parameter GPIO_TRANSFER_STATE_DR13 = 61;
	parameter GPIO_TRANSFER_STATE_DR14 = 62;
	parameter GPIO_TRANSFER_STATE_DR15 = 63;
	parameter GPIO_TRANSFER_STATE_DR16 = 64;
	parameter GPIO_TRANSFER_STATE_DR17 = 65;
	parameter GPIO_TRANSFER_STATE_DR18 = 66;
	parameter GPIO_TRANSFER_STATE_DR19 = 67;
	parameter GPIO_TRANSFER_STATE_DR20 = 68;
	parameter GPIO_TRANSFER_STATE_DR21 = 69;
	parameter GPIO_TRANSFER_STATE_DR22 = 70;
	parameter GPIO_TRANSFER_STATE_DR23 = 71;

	parameter GPIO_TRANSFER_STATE_RX01 = 128;
	parameter GPIO_TRANSFER_STATE_RX02 = 129;
	parameter GPIO_TRANSFER_STATE_RX03 = 130;
	parameter GPIO_TRANSFER_STATE_RX04 = 131;
	parameter GPIO_TRANSFER_STATE_RX05 = 132;
	parameter GPIO_TRANSFER_STATE_RX06 = 133;
	parameter GPIO_TRANSFER_STATE_RX07 = 134;
	parameter GPIO_TRANSFER_STATE_RX08 = 135;
	parameter GPIO_TRANSFER_STATE_RX09 = 136;
	parameter GPIO_TRANSFER_STATE_RX10 = 137;
	parameter GPIO_TRANSFER_STATE_RX11 = 138;
	parameter GPIO_TRANSFER_STATE_RX12 = 139;
	parameter GPIO_TRANSFER_STATE_RX13 = 140;
	parameter GPIO_TRANSFER_STATE_RX14 = 141;
	parameter GPIO_TRANSFER_STATE_RX15 = 142;
	parameter GPIO_TRANSFER_STATE_RX16 = 143;
	parameter GPIO_TRANSFER_STATE_RX17 = 144;
	parameter GPIO_TRANSFER_STATE_RX18 = 145;
	parameter GPIO_TRANSFER_STATE_RX19 = 146;
	parameter GPIO_TRANSFER_STATE_RX20 = 147;
	parameter GPIO_TRANSFER_STATE_RX21 = 148;
	parameter GPIO_TRANSFER_STATE_RX22 = 149;
	parameter GPIO_TRANSFER_STATE_RX23 = 150;
	parameter GPIO_TRANSFER_STATE_RX24 = 151;
	parameter GPIO_TRANSFER_STATE_RX25 = 152;
	parameter GPIO_TRANSFER_STATE_RX26 = 153;
	parameter GPIO_TRANSFER_STATE_RX27 = 154;
	parameter GPIO_TRANSFER_STATE_RX28 = 155;
	parameter GPIO_TRANSFER_STATE_RX29 = 156;
	parameter GPIO_TRANSFER_STATE_RX30 = 157;


	reg [7:0] control_state = 0;
	reg [7:0] startup_timer = 0;
	reg [31:0] operation_timeout = 0;

	always @(posedge peripheral_clock) begin
		if (peripheral_reset) begin
			control_state <= GPIO_INITIALIZE_STATE_01;
		end else begin
			case (control_state)
				GPIO_INITIALIZE_STATE_01: begin
					i2c_ctl_read <= 0;
					i2c_ctl_write <= 0;

					startup_timer <= startup_timer + 1;
					if (startup_timer > 10) begin
						control_state <= GPIO_TRANSFER_STATE_IDLE;
					end
				end
				GPIO_TRANSFER_STATE_IDLE: begin
					// Latch drive data
					gpio_in_reg <= gpio_in;
					gpio_direction_reg <= gpio_direction;

					// Signal next digit state
					digit_sync_reg <= 1;

					// Start transaction
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= (GPIO_CONTROLLER_ADDRESS << 1);	// Write
					i2c_ctl_start <= 1;
					control_state <= GPIO_TRANSFER_STATE_TX01;
				end
				GPIO_TRANSFER_STATE_TX01: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						i2c_ctl_start <= 0;
						digit_sync_reg <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_TX02;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							digit_sync_reg <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_TX02: begin
					// Send start register address
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= GPIO_CTRL_REG_DAT_OUT1;
					control_state <= GPIO_TRANSFER_STATE_TX03;
				end
				GPIO_TRANSFER_STATE_TX03: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_TX04;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_TX04: begin
					// Send first byte of register data
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write[7] <= gpio_in_reg[0];
					i2c_data_write[6] <= gpio_in_reg[1];
					i2c_data_write[5] <= gpio_in_reg[2];
					i2c_data_write[4] <= gpio_in_reg[3];
					i2c_data_write[3] <= gpio_in_reg[4];
					i2c_data_write[2] <= gpio_in_reg[5];
					i2c_data_write[1] <= gpio_in_reg[6];
					i2c_data_write[0] <= gpio_in_reg[7];
					control_state <= GPIO_TRANSFER_STATE_TX05;
				end
				GPIO_TRANSFER_STATE_TX05: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_TX06;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_TX06: begin
					// Stop transaction
					operation_timeout <= 0;
					i2c_ctl_stop <= 1;
					control_state <= GPIO_TRANSFER_STATE_TX07;
				end
				GPIO_TRANSFER_STATE_TX07: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_stop <= 0;
						control_state <= GPIO_TRANSFER_STATE_TX08;
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_TX08: begin
					// Start transaction
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= (GPIO_CONTROLLER_ADDRESS << 1);	// Write
					i2c_ctl_start <= 1;
					control_state <= GPIO_TRANSFER_STATE_TX09;
				end
				GPIO_TRANSFER_STATE_TX09: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						i2c_ctl_start <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_TX10;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_TX10: begin
					// Send start register address
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= (GPIO_CTRL_REG_DAT_OUT1 + 1);
					control_state <= GPIO_TRANSFER_STATE_TX11;
				end
				GPIO_TRANSFER_STATE_TX11: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_TX12;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_TX12: begin
					// Send second byte of register data
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= gpio_in_reg[15:8];
					control_state <= GPIO_TRANSFER_STATE_TX13;
				end
				GPIO_TRANSFER_STATE_TX13: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_TX14;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_TX14: begin
					// Stop transaction
					operation_timeout <= 0;
					i2c_ctl_stop <= 1;
					control_state <= GPIO_TRANSFER_STATE_TX15;
				end
				GPIO_TRANSFER_STATE_TX15: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_stop <= 0;
						control_state <= GPIO_TRANSFER_STATE_TX16;
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_TX16: begin
					// Start transaction
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= (GPIO_CONTROLLER_ADDRESS << 1);	// Write
					i2c_ctl_start <= 1;
					control_state <= GPIO_TRANSFER_STATE_TX17;
				end
				GPIO_TRANSFER_STATE_TX17: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						i2c_ctl_start <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_TX18;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_TX18: begin
					// Send start register address
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= (GPIO_CTRL_REG_DAT_OUT1 + 2);
					control_state <= GPIO_TRANSFER_STATE_TX19;
				end
				GPIO_TRANSFER_STATE_TX19: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_TX20;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_TX20: begin
					// Send third byte of register data
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write[7:2] <= 0;
					i2c_data_write[1:0] <= gpio_in_reg[17:16];
					control_state <= GPIO_TRANSFER_STATE_TX21;
				end
				GPIO_TRANSFER_STATE_TX21: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_TX22;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_TX22: begin
					// Stop transaction
					operation_timeout <= 0;
					i2c_ctl_stop <= 1;
					control_state <= GPIO_TRANSFER_STATE_TX23;
				end
				GPIO_TRANSFER_STATE_TX23: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_stop <= 0;
						control_state <= GPIO_TRANSFER_STATE_DR00;
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_DR00: begin
					// Start transaction
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= (GPIO_CONTROLLER_ADDRESS << 1);	// Write
					i2c_ctl_start <= 1;
					control_state <= GPIO_TRANSFER_STATE_DR01;
				end
				GPIO_TRANSFER_STATE_DR01: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						i2c_ctl_start <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_DR02;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_DR02: begin
					// Send start register address
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= GPIO_CTRL_REG_DIR1;
					control_state <= GPIO_TRANSFER_STATE_DR03;
				end
				GPIO_TRANSFER_STATE_DR03: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_DR04;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_DR04: begin
					// Send first byte of register data
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write[7] <= gpio_direction_reg[0];
					i2c_data_write[6] <= gpio_direction_reg[1];
					i2c_data_write[5] <= gpio_direction_reg[2];
					i2c_data_write[4] <= gpio_direction_reg[3];
					i2c_data_write[3] <= gpio_direction_reg[4];
					i2c_data_write[2] <= gpio_direction_reg[5];
					i2c_data_write[1] <= gpio_direction_reg[6];
					i2c_data_write[0] <= gpio_direction_reg[7];
					control_state <= GPIO_TRANSFER_STATE_DR05;
				end
				GPIO_TRANSFER_STATE_DR05: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_DR06;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_DR06: begin
					// Stop transaction
					operation_timeout <= 0;
					i2c_ctl_stop <= 1;
					control_state <= GPIO_TRANSFER_STATE_DR07;
				end
				GPIO_TRANSFER_STATE_DR07: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_stop <= 0;
						control_state <= GPIO_TRANSFER_STATE_DR08;
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_DR08: begin
					// Start transaction
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= (GPIO_CONTROLLER_ADDRESS << 1);	// Write
					i2c_ctl_start <= 1;
					control_state <= GPIO_TRANSFER_STATE_DR09;
				end
				GPIO_TRANSFER_STATE_DR09: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						i2c_ctl_start <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_DR10;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_DR10: begin
					// Send start register address
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= (GPIO_CTRL_REG_DIR1 + 1);
					control_state <= GPIO_TRANSFER_STATE_DR11;
				end
				GPIO_TRANSFER_STATE_DR11: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_DR12;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_DR12: begin
					// Send second byte of register data
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= gpio_direction_reg[15:8];
					control_state <= GPIO_TRANSFER_STATE_DR13;
				end
				GPIO_TRANSFER_STATE_DR13: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_DR14;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_DR14: begin
					// Stop transaction
					operation_timeout <= 0;
					i2c_ctl_stop <= 1;
					control_state <= GPIO_TRANSFER_STATE_DR15;
				end
				GPIO_TRANSFER_STATE_DR15: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_stop <= 0;
						control_state <= GPIO_TRANSFER_STATE_DR16;
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_DR16: begin
					// Start transaction
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= (GPIO_CONTROLLER_ADDRESS << 1);	// Write
					i2c_ctl_start <= 1;
					control_state <= GPIO_TRANSFER_STATE_DR17;
				end
				GPIO_TRANSFER_STATE_DR17: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						i2c_ctl_start <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_DR18;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_DR18: begin
					// Send start register address
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= (GPIO_CTRL_REG_DIR1 + 2);
					control_state <= GPIO_TRANSFER_STATE_DR19;
				end
				GPIO_TRANSFER_STATE_DR19: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_DR20;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_DR20: begin
					// Send third byte of register data
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write[7:2] <= 0;
					i2c_data_write[1:0] <= gpio_direction_reg[17:16];
					control_state <= GPIO_TRANSFER_STATE_DR21;
				end
				GPIO_TRANSFER_STATE_DR21: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_DR22;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_DR22: begin
					// Stop transaction
					operation_timeout <= 0;
					i2c_ctl_stop <= 1;
					control_state <= GPIO_TRANSFER_STATE_DR23;
				end
				GPIO_TRANSFER_STATE_DR23: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_stop <= 0;
						if (gpio_direction == 18'h3ffff) begin
							// Skip GPIO input read since all GPIO
							// lines have been set to output mode
							control_state <= GPIO_TRANSFER_STATE_IDLE;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_RX01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_RX01: begin
					// Start write portion of transaction
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= (GPIO_CONTROLLER_ADDRESS << 1);	// Write
					i2c_ctl_start <= 1;
					control_state <= GPIO_TRANSFER_STATE_RX02;
				end
				GPIO_TRANSFER_STATE_RX02: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						i2c_ctl_start <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_RX03;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_RX03: begin
					// Send start register address
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= GPIO_DAT_STAT1;
					control_state <= GPIO_TRANSFER_STATE_RX04;
				end
				GPIO_TRANSFER_STATE_RX04: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_RX05;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_RX05: begin
					// Start read portion of transaction
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= (GPIO_CONTROLLER_ADDRESS << 1) | 1;	// Read
					i2c_ctl_start <= 1;
					control_state <= GPIO_TRANSFER_STATE_RX06;
				end
				GPIO_TRANSFER_STATE_RX06: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						i2c_ctl_start <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_RX07;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_RX07: begin
					// Receive first byte of register data
					operation_timeout <= 0;
					i2c_ctl_read <= 1;
					i2c_ctl_ack <= 1;	// Signal controller to stop sending bytes after this one is received
					control_state <= GPIO_TRANSFER_STATE_RX08;
				end
				GPIO_TRANSFER_STATE_RX08: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_read <= 0;
						i2c_ctl_ack <= 0;
						gpio_out_reg[7] <= i2c_data_read[0];
						gpio_out_reg[6] <= i2c_data_read[1];
						gpio_out_reg[5] <= i2c_data_read[2];
						gpio_out_reg[4] <= i2c_data_read[3];
						gpio_out_reg[3] <= i2c_data_read[4];
						gpio_out_reg[2] <= i2c_data_read[5];
						gpio_out_reg[1] <= i2c_data_read[6];
						gpio_out_reg[0] <= i2c_data_read[7];
						control_state <= GPIO_TRANSFER_STATE_RX09;
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_RX09: begin
					// Stop transaction
					operation_timeout <= 0;
					i2c_ctl_stop <= 1;
					control_state <= GPIO_TRANSFER_STATE_RX10;
				end
				GPIO_TRANSFER_STATE_RX10: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_stop <= 0;
						control_state <= GPIO_TRANSFER_STATE_RX11;
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_RX11: begin
					// Start write portion of transaction
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= (GPIO_CONTROLLER_ADDRESS << 1);	// Write
					i2c_ctl_start <= 1;
					control_state <= GPIO_TRANSFER_STATE_RX12;
				end
				GPIO_TRANSFER_STATE_RX12: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						i2c_ctl_start <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_RX13;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_RX13: begin
					// Send start register address
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= (GPIO_DAT_STAT1 + 1);
					control_state <= GPIO_TRANSFER_STATE_RX14;
				end
				GPIO_TRANSFER_STATE_RX14: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_RX15;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_RX15: begin
					// Start read portion of transaction
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= (GPIO_CONTROLLER_ADDRESS << 1) | 1;	// Read
					i2c_ctl_start <= 1;
					control_state <= GPIO_TRANSFER_STATE_RX16;
				end
				GPIO_TRANSFER_STATE_RX16: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						i2c_ctl_start <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_RX17;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_RX17: begin
					// Receive second byte of register data
					operation_timeout <= 0;
					i2c_ctl_read <= 1;
					i2c_ctl_ack <= 1;	// Signal controller to stop sending bytes after this one is received
					control_state <= GPIO_TRANSFER_STATE_RX18;
				end
				GPIO_TRANSFER_STATE_RX18: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_read <= 0;
						i2c_ctl_ack <= 0;
						gpio_out_reg[15:8] <= i2c_data_read;
						control_state <= GPIO_TRANSFER_STATE_RX19;
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_RX19: begin
					// Stop transaction
					operation_timeout <= 0;
					i2c_ctl_stop <= 1;
					control_state <= GPIO_TRANSFER_STATE_RX20;
				end
				GPIO_TRANSFER_STATE_RX20: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_stop <= 0;
						control_state <= GPIO_TRANSFER_STATE_RX21;
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_RX21: begin
					// Start write portion of transaction
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= (GPIO_CONTROLLER_ADDRESS << 1);	// Write
					i2c_ctl_start <= 1;
					control_state <= GPIO_TRANSFER_STATE_RX22;
				end
				GPIO_TRANSFER_STATE_RX22: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						i2c_ctl_start <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_RX23;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_RX23: begin
					// Send start register address
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= (GPIO_DAT_STAT1 + 2);
					control_state <= GPIO_TRANSFER_STATE_RX24;
				end
				GPIO_TRANSFER_STATE_RX24: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_RX25;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_RX25: begin
					// Start read portion of transaction
					operation_timeout <= 0;
					i2c_ctl_write <= 1;
					i2c_data_write <= (GPIO_CONTROLLER_ADDRESS << 1) | 1;	// Read
					i2c_ctl_start <= 1;
					control_state <= GPIO_TRANSFER_STATE_RX26;
				end
				GPIO_TRANSFER_STATE_RX26: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_write <= 0;
						i2c_ctl_start <= 0;
						if (!i2c_sta_ack) begin
							control_state <= GPIO_TRANSFER_STATE_RX27;
						end else begin
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_RX27: begin
					// Receive third byte of register data
					operation_timeout <= 0;
					i2c_ctl_read <= 1;
					i2c_ctl_ack <= 1;	// Signal controller to stop sending bytes after this one is received
					control_state <= GPIO_TRANSFER_STATE_RX28;
				end
				GPIO_TRANSFER_STATE_RX28: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_read <= 0;
						i2c_ctl_ack <= 0;
						gpio_out_reg[17:16] <= i2c_data_read[1:0];
						control_state <= GPIO_TRANSFER_STATE_RX29;
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_RX29: begin
					// Stop transaction
					operation_timeout <= 0;
					i2c_ctl_stop <= 1;
					control_state <= GPIO_TRANSFER_STATE_RX30;
				end
				GPIO_TRANSFER_STATE_RX30: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_stop <= 0;
						control_state <= GPIO_TRANSFER_STATE_IDLE;
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_write <= 0;
							i2c_ctl_start <= 0;
							control_state <= GPIO_TRANSFER_STATE_ER01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				GPIO_TRANSFER_STATE_ER01: begin
					// Abort transaction
					i2c_ctl_stop <= 1;
					control_state <= GPIO_TRANSFER_STATE_ER02;
				end
				GPIO_TRANSFER_STATE_ER02: begin
					if (i2c_cmd_ack || i2c_sta_arb_lost) begin
						i2c_ctl_stop <= 0;
						control_state <= GPIO_INITIALIZE_STATE_01;
					end else begin
						if (operation_timeout > OPERATION_TIMEOUT_CYCLES) begin
							i2c_ctl_stop <= 0;
							control_state <= GPIO_INITIALIZE_STATE_01;
						end else begin
							operation_timeout <= operation_timeout + 1;
						end
					end
				end
				default: begin
					control_state <= GPIO_TRANSFER_STATE_ER01;
				end
			endcase
		end
	end

endmodule
