// © 2022 Raptor Engineering, LLC
//
// Released under the terms of the LGPL v3+
// See the LICENSE file for full details

module smd_font_rom(
	input [6:0] address,
	output [13:0] data,
	input clk
	);

	reg [13:0] rom [127:0];

	reg [13:0] data_reg = 0;
	assign data = data_reg;

	initial begin
		$readmemh("font_map.hex", rom);
	end

	always @(posedge clk) begin
		data_reg <= rom[address];
	end
endmodule

module smd_driver(
	output wire [13:0] anodes,
	output wire [3:0] cathodes,

	input [6:0] display_byte_1,
	input [6:0] display_byte_2,
	input [6:0] display_byte_3,
	input [6:0] display_byte_4,

	input digit_sync,
	input clk
	);

	wire [13:0] rom_data;
	reg [2:0] display_sequencer = 0;
	reg [3:0] cathode_reg = 0;
	reg [3:0] cathode_reg_delayed = 0;
	reg [6:0] display_sequencer_byte = 0;
	reg digit_sync_reg = 0;
	reg digit_sync_reg_prev = 0;

	assign cathodes = cathode_reg_delayed;
	assign anodes[13] = rom_data[0];
	assign anodes[12] = rom_data[1];
	assign anodes[11] = rom_data[2];
	assign anodes[10] = rom_data[3];
	assign anodes[9] = rom_data[4];
	assign anodes[8] = rom_data[5];
	assign anodes[7] = rom_data[6];
	assign anodes[6] = rom_data[7];
	assign anodes[5] = rom_data[8];
	assign anodes[4] = rom_data[9];
	assign anodes[3] = rom_data[10];
	assign anodes[2] = rom_data[11];
	assign anodes[1] = rom_data[12];
	assign anodes[0] = rom_data[13];

	always @(posedge clk) begin
		case (display_sequencer)
			0: display_sequencer_byte <= display_byte_1;
			1: display_sequencer_byte <= display_byte_1;
			2: display_sequencer_byte <= display_byte_2;
			3: display_sequencer_byte <= display_byte_2;
			4: display_sequencer_byte <= display_byte_3;
			5: display_sequencer_byte <= display_byte_3;
			6: display_sequencer_byte <= display_byte_4;
			7: display_sequencer_byte <= display_byte_4;
		endcase
	end

	always @(posedge clk) begin
		case (display_sequencer)
			0: cathode_reg <= 4'b1000;
			1: cathode_reg <= 4'b0000;
			2: cathode_reg <= 4'b0100;
			3: cathode_reg <= 4'b0000;
			4: cathode_reg <= 4'b0010;
			5: cathode_reg <= 4'b0000;
			6: cathode_reg <= 4'b0001;
			7: cathode_reg <= 4'b0000;
		endcase
		cathode_reg_delayed <= cathode_reg;
	end

	// Instantiate font ROM
	smd_font_rom smd_font_rom(
		.address(display_sequencer_byte),
		.data(rom_data),
		.clk(clk)
	);

	always @(posedge clk) begin
		if (digit_sync_reg && !digit_sync_reg_prev) begin
			display_sequencer <= display_sequencer + 1;
		end

		digit_sync_reg <= digit_sync;
		digit_sync_reg_prev <= digit_sync_reg;
	end
endmodule

module smd_hex_output(
	output wire [13:0] anodes,
	output wire [3:0] cathodes,

	input [32:0] display_word,
	input raw_mode,

	input digit_sync,
	input clk
	);

	reg [6:0] display_byte_1 = 0;
	reg [6:0] display_byte_2 = 0;
	reg [6:0] display_byte_3 = 0;
	reg [6:0] display_byte_4 = 0;

	smd_driver smd_driver(
		.anodes(anodes),
		.cathodes(cathodes),

		.display_byte_1(display_byte_1),
		.display_byte_2(display_byte_2),
		.display_byte_3(display_byte_3),
		.display_byte_4(display_byte_4),

		.digit_sync(digit_sync),
		.clk(clk)
	);

	always @(posedge clk) begin
		if (raw_mode) begin
			display_byte_1 <= display_word[7:0];
		end else begin
			if (display_word[3:0] > 4'h9) begin
				display_byte_1 <= display_word[3:0] + 7'h37;
			end else begin
				display_byte_1 <= display_word[3:0] + 7'h30;
			end
		end
	end

	always @(posedge clk) begin
		if (raw_mode) begin
			display_byte_2 <= display_word[15:8];
		end else begin
			if (display_word[7:4] > 4'h9) begin
				display_byte_2 <= display_word[7:4] + 7'h37;
			end else begin
				display_byte_2 <= display_word[7:4] + 7'h30;
			end
		end
	end

	always @(posedge clk) begin
		if (raw_mode) begin
			display_byte_3 <= display_word[23:16];
		end else begin
			if (display_word[11:8] > 4'h9) begin
				display_byte_3 <= display_word[11:8] + 7'h37;
			end else begin
				display_byte_3 <= display_word[11:8] + 7'h30;
			end
		end
	end

	always @(posedge clk) begin
		if (raw_mode) begin
			display_byte_4 <= display_word[31:24];
		end else begin
			if (display_word[15:12] > 4'h9) begin
				display_byte_4 <= display_word[15:12] + 7'h37;
			end else begin
				display_byte_4 <= display_word[15:12] + 7'h30;
			end
		end
	end
endmodule
